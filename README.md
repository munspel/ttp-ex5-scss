# ttp-ex5-css + scss

## Введение с CSS + SSCS

### Виды селекторов CSS

- селекторы тегов
- селекторы классов
- селекторы идентификаторов
- контекстные селекторы
- смежные селекторы
- сестринские селекторы
## Консольные команды

Для установки зависимостей:
```shell script
npm install
```
Для розработки (file watch + live reload) 
```shell script
npm run watch
```

## Пример

https://mesothelial-speeder.000webhostapp.com/

## Полезные ссылки

- Pipeline validator [https://bitbucket-pipelines.prod.public.atl-paas.net/validator]
- Команда npm run [https://frontender.info/task_automation_with_npm_run/]

### BEM

- https://nicothin.pro/idiomatic-pre-CSS/#bem-cool
- http://yoksel.github.io/easy-markup/bem-rules/
- https://www.smashingmagazine.com/2016/06/battling-bem-extended-edition-common-problems-and-how-to-avoid-them/

